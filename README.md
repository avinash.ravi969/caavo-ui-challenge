# Steps For Running the App
1. clone the app from the gitlab
2. npm install
3. ng serve --o

# Flow of the app
1. Sidebar with three services Amazon, Netflix, Hulu
2. Click on each service to view corresponding details
3. Netflix has additonal tabs view which will show corresponding details by clicking on the tabs
4. In Netflix "MOM's" tab click on the dexter to see it's seasons related data
5. By clicking on the View seasons button it will naviate to a new page which will show it's corresponding seasons information

# Features
1. Reponsive web-app
2. Included http-loader for slow networks

# Check Site At
https://caavo-ui-challenge.netlify.com/