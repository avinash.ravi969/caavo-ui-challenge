import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

/**
 * service to manage amazon json data
 *
 * @export
 * @class AmazonService
 */
@Injectable({
  providedIn: 'root'
})
export class AmazonService {

  /**
   *Creates an instance of AmazonService.

   * @param {HttpClient} http
   * @memberof AmazonService
   */
  constructor(
    private http: HttpClient
  ) { }


  /**
   * Get the details of amazon json data
   *
   * @returns {Observable<any>}
   * @memberof AmazonService
   */
  public getAmazonJSONFeed(): Observable<any> {
    return this.http.get("../../../assets/feeds/amazon.json")
  }
}
