import { TestBed, inject } from '@angular/core/testing';

import { DexterSeasonsService } from './dexter-seasons.service';

describe('DexterSeasonsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DexterSeasonsService]
    });
  });

  it('should be created', inject([DexterSeasonsService], (service: DexterSeasonsService) => {
    expect(service).toBeTruthy();
  }));
});
