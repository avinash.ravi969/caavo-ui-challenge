import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

/**
 * Service to manage dexter season episodes
 *
 * @export
 * @class DexterSeasonsService
 */
@Injectable({
  providedIn: 'root'
})
export class DexterSeasonsService {

  /**
   *Creates an instance of DexterSeasonsService.
  
   * @param {HttpClient} http
   * @memberof DexterSeasonsService
   */
  constructor(
    private http: HttpClient
  ) { }

  /**
   * Get Dexter season 1 episode details
   *
   * @returns {Observable<any>}
   * @memberof DexterSeasonsService
   */
  public getDexterSeason1Episodes(): Observable<any> {
    return this.http.get("../../../assets/feeds/dexter_season_1_episodes.json")
  }

  /**
   * Get dexter season 2 episode details
   *
   * @returns {Observable<any>}
   * @memberof DexterSeasonsService
   */
  public getDexterSeason2Episodes(): Observable<any> {
    return this.http.get("../../../assets/feeds/dexter_season_2_episodes.json")
  }

  /**
   * Get dexter season 3 episode details
   *
   * @returns {Observable<any>}
   * @memberof DexterSeasonsService
   */
  public getDexterSeason3Episodes(): Observable<any> {
    return this.http.get("../../../assets/feeds/dexter_season_3_episodes.json")
  }
}
