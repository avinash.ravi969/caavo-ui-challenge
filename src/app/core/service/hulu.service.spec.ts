import { TestBed, inject } from '@angular/core/testing';

import { HuluService } from './hulu.service';

describe('HuluService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HuluService]
    });
  });

  it('should be created', inject([HuluService], (service: HuluService) => {
    expect(service).toBeTruthy();
  }));
});
