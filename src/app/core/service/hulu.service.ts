import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


/**
 * Service to manage hulu json data
 *
 * @export
 * @class HuluService
 */
@Injectable({
  providedIn: 'root'
})
export class HuluService {

  /**
   *Creates an instance of HuluService.

   * @param {HttpClient} http
   * @memberof HuluService
   */
  constructor(
    private http: HttpClient
  ) { }

  /**
   * Get details of hulu json data
   *
   * @returns {Observable<any>}
   * @memberof HuluService
   */
  public getHuluJSONData(): Observable<any> {
    return this.http.get("../../../assets/feeds/hulu.json")
  }
}
