import { TestBed, inject } from '@angular/core/testing';

import { NetflixusaService } from './netflixusa.service';

describe('NetflixusaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NetflixusaService]
    });
  });

  it('should be created', inject([NetflixusaService], (service: NetflixusaService) => {
    expect(service).toBeTruthy();
  }));
});
