import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

/**
 * Service to manage netflix usa json data
 *
 * @export
 * @class NetflixusaService
 */
@Injectable({
  providedIn: 'root'
})
export class NetflixusaService {

  /**
   *Creates an instance of NetflixusaService.
   
   * @param {HttpClient} http
   * @memberof NetflixusaService
   */
  constructor(
    private http: HttpClient
  ) { }

  /**
   * Get details of netflix dad's data from json file
   *
   * @returns {Observable<any>}
   * @memberof NetflixusaService
   */
  public getNetflixDadJSONData(): Observable<any> {
    return this.http.get("../../../assets/feeds/netflix_dad.json")
  }

  /**
   * Get details of netflix mom's data from json file
   *
   * @returns {Observable<any>}
   * @memberof NetflixusaService
   */
  public getNetflixMomJSONData(): Observable<any> {
    return this.http.get("../../../assets/feeds/netflix_mom.json")
  }

  /**
   * Get details of netflix stephen data fron stephen json file
   *
   * @returns {Observable<any>}
   * @memberof NetflixusaService
   */
  public getNetflixStephenJSONData(): Observable<any> {
    return this.http.get("../../../assets/feeds/netflix_stephen.json")
  }
}
