import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

/**
 * Service to manage accounts data
 *
 * @export
 * @class SidebarService
 */
@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  /**
   *Creates an instance of SidebarService.
  
   * @param {HttpClient} http
   * @memberof SidebarService
   */
  constructor(
    private http: HttpClient
  ) { }

  /**
   * Get Details of Accounts from accounts json file
   *
   * @returns {Observable<any>}
   * @memberof SidebarService
   */
  public getSideBarJSONFeed(): Observable<any> {
    return this.http.get("../../../assets/feeds/accounts.json")
  }
}
