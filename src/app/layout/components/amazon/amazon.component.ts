import { Component, OnInit } from '@angular/core';
import { AmazonService } from 'src/app/core/service/amazon.service';

/**
 * Handles all amazon related functions
 *
 * @export
 * @class AmazonComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-amazon',
  templateUrl: './amazon.component.html',
  styleUrls: ['./amazon.component.scss']
})
export class AmazonComponent implements OnInit {
  amazonData: any;
  changeText: boolean;

  /**
   *Creates an instance of AmazonComponent.
   * @param {AmazonService} amazonService
   * @memberof AmazonComponent
   */
  constructor(
    private amazonService: AmazonService
  ) { }

  /**
   * On init get amazon feed data
   *
   * @memberof AmazonComponent
   */
  ngOnInit() {
    this.amazonService.getAmazonJSONFeed().subscribe(data => {
      this.amazonData = data;
    })
  }

}
