import { Component, OnInit } from '@angular/core';
import { HuluService } from 'src/app/core/service/hulu.service';

/**
 * Handle all hulu related functions
 *
 * @export
 * @class HuluComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-hulu',
  templateUrl: './hulu.component.html',
  styleUrls: ['./hulu.component.scss']
})
export class HuluComponent implements OnInit {
  huluData: any;

  /**
   *Creates an instance of HuluComponent.
   * @param {HuluService} huluService
   * @memberof HuluComponent
   */
  constructor(
    private huluService: HuluService
  ) { }

  /**
   * On init load hulu data
   *
   * @memberof HuluComponent
   */
  ngOnInit() {
    this.huluService.getHuluJSONData().subscribe(data => {
      this.huluData = data;
    })
  }

}
