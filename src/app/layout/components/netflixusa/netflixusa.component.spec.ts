import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NetflixusaComponent } from './netflixusa.component';

describe('NetflixusaComponent', () => {
  let component: NetflixusaComponent;
  let fixture: ComponentFixture<NetflixusaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NetflixusaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NetflixusaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
