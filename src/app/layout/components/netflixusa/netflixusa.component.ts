import { Component, OnInit } from '@angular/core';
import { NetflixusaService } from 'src/app/core/service/netflixusa.service';
import { Router } from '@angular/router';

/**
 * Handle netflix related actions
 *
 * @export
 * @class NetflixusaComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-netflixusa',
  templateUrl: './netflixusa.component.html',
  styleUrls: ['./netflixusa.component.scss']
})
export class NetflixusaComponent implements OnInit {
  netflixDadData: any;
  netflixMomData: any;
  netflixStephenData: any;
  show: any;

  /**
   *Creates an instance of NetflixusaComponent.
   * @param {NetflixusaService} netflixService
   * @param {Router} router
   * @memberof NetflixusaComponent
   */
  constructor(
    private netflixService: NetflixusaService,
    public router: Router
  ) { }

  /**
   * Load netflix related data on init
   *
   * @memberof NetflixusaComponent
   */
  ngOnInit() {
    this.show = false;
    this.netflixService.getNetflixDadJSONData().subscribe(data => {
      this.netflixDadData = data;
    });

    this.netflixService.getNetflixMomJSONData().subscribe(data => {
      this.netflixMomData = data;
    });

    this.netflixService.getNetflixStephenJSONData().subscribe(data => {
      this.netflixStephenData = data;
    });
  }

}
