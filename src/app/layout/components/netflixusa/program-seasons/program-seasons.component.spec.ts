import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramSeasonsComponent } from './program-seasons.component';

describe('ProgramSeasonsComponent', () => {
  let component: ProgramSeasonsComponent;
  let fixture: ComponentFixture<ProgramSeasonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgramSeasonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramSeasonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
