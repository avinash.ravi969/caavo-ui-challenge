import { Component, OnInit } from '@angular/core';
import { DexterSeasonsService } from 'src/app/core/service/dexter-seasons.service';

/**
 * Handle Program Seasons Related functions
 *
 * @export
 * @class ProgramSeasonsComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-program-seasons',
  templateUrl: './program-seasons.component.html',
  styleUrls: ['./program-seasons.component.scss']
})
export class ProgramSeasonsComponent implements OnInit {
  season1Data: any;
  season2Data: any;
  season3Data: any;
  show: any;

  /**
   *Creates an instance of ProgramSeasonsComponent.
   * @param {DexterSeasonsService} seasonsService
   * @memberof ProgramSeasonsComponent
   */
  constructor(
    private seasonsService: DexterSeasonsService
  ) { }

  /**
   * on init load all season program related data
   *
   * @memberof ProgramSeasonsComponent
   */
  ngOnInit() {
    this.show = true;
    this.seasonsService.getDexterSeason1Episodes().subscribe(data => {
      this.season1Data = data;
      console.log("Season-1 Data", this.season1Data);

    });

    this.seasonsService.getDexterSeason2Episodes().subscribe(data => {
      this.season2Data = data;
      console.log("Season-2 Data", this.season2Data);
    });

    this.seasonsService.getDexterSeason3Episodes().subscribe(data => {
      this.season3Data = data;
    });
  }

}
