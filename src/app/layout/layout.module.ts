import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './components/home/home.component';
import { LayoutComponent } from './layout.component';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';
import { AmazonComponent } from './components/amazon/amazon.component';
import { HuluComponent } from './components/hulu/hulu.component';
import { NetflixusaComponent } from './components/netflixusa/netflixusa.component';
import { ProgramSeasonsComponent } from './components/netflixusa/program-seasons/program-seasons.component';
import { RouterModule } from '@angular/router';
import { layoutRoutes } from './layouts.routes';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    SharedModule,
    RouterModule.forChild(layoutRoutes),
    //NetflixusaModule
  ],
  declarations: [
    HomeComponent,
    LayoutComponent,
    AmazonComponent,
    HuluComponent,
    NetflixusaComponent,
    ProgramSeasonsComponent
  ],
  exports: [
    RouterModule
  ]
})
export class LayoutModule { }
