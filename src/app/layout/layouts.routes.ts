import { Routes } from "@angular/router";
import { LayoutComponent } from "./layout.component";
import { HomeComponent } from "./components/home/home.component";
import { AmazonComponent } from "./components/amazon/amazon.component";
import { HuluComponent } from "./components/hulu/hulu.component";
import { NetflixusaComponent } from "./components/netflixusa/netflixusa.component";
import { ProgramSeasonsComponent } from "./components/netflixusa/program-seasons/program-seasons.component";

/**
 * Routes for all layouts
 * 
 */
export const layoutRoutes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'amazon', component: AmazonComponent },
            { path: 'hulu', component: HuluComponent },
            {
                path: 'netflixusa',
                component: NetflixusaComponent,
                children: [
                    { path: 'seasons', component: ProgramSeasonsComponent },
                    { path: '', redirectTo:'netflixusa', pathMatch:"full" }
                ]
            }
        ]
    }
];