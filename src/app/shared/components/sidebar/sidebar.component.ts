import { Component, OnInit } from '@angular/core';
import { SidebarService } from 'src/app/core/service/sidebar.service';

/**
 * Handle Sidebar realated functions
 *
 * @export
 * @class SidebarComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  accountsFeedData: any;

  /**
   *Creates an instance of SidebarComponent.
   * @param {SidebarService} sidebarService
   * @memberof SidebarComponent
   */
  constructor(
    private sidebarService: SidebarService
  ) { }

  /**
   * Load sidebar components
   *
   * @memberof SidebarComponent
   */
  ngOnInit() {
    this.sidebarService.getSideBarJSONFeed().subscribe(data => {
      this.accountsFeedData = data;
    });
  }

}
